package coreseformas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

public class DiagonalPanel extends JPanel {
    
    private Color color1 = new Color(255, 0, 0);
    private Color color2 = new Color(0, 0, 255);
    private Color color3 = new Color(0, 0, 0);
    private Color color4 = new Color(240, 240, 240);
    
    private int i = 1;
    private int n = 1;
    
    public DiagonalPanel(int x, int y)
    {
        this.i = x;
        this.n = y;
    }
    
    public void setBaixo (int b)
    {
        this.n = b;
    }
    
    public void setI (int x)
    {
        this.i = x;
    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        int w = getWidth();
        int h = getHeight();
        switch(i)
        {
            case 1:
                g2d.setPaint(color1);
                break;
                
            case 2:
                g2d.setPaint(color2);
                break;
                
            case 3:
                g2d.setPaint(color3);
                break;
                
            case 4:
                g2d.setPaint(color4);
                break;
                
            default:
                break;
        }
        switch(n)
        {
            case 1:
                g2d.fillPolygon(new int[] {0, w, 0}, new int[] {0, h, h}, 3);
                break;
                
            case 2:
                g2d.fillPolygon(new int[] {0, w, w}, new int[] {0, 0, h}, 3);
                break;
                
            case 3:
                g2d.fillPolygon(new int[] {w, w, 0}, new int[] {0, h, h}, 3);
                break;
                
            case 4:
                g2d.fillPolygon(new int[] {0, w, 0}, new int[] {0, 0, h}, 3);
                break;
                
            default:
                break;
        }

    }
    

    
/*
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame();
                GradientPanel panel = new GradientPanel();
                frame.add(panel);
                frame.setSize(200, 200);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
*/
}